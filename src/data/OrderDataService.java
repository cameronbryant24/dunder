package data;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import beans.Order;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class OrderDataService implements DataAccessInterface<Order> {

	private Connection conn = null;
	private String url = "jdbc:mysql://localhost:3306/testapp";
	private String username = "root";
	private String password = "9286148990";
	
	public OrderDataService() {
		
	}
	
	public List<Order> findAll() {
		
		String sql = "Select * FROM ORDERS";
		List<Order> orders = new ArrayList<Order>();
		try {
			//connect to the database
			conn = DriverManager.getConnection(url, username, password);
			
			//execute SQL Query and loop over result set
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				orders.add(new Order(rs.getInt("ID"), 
						rs.getString("ORDER_NO"), 
						rs.getString("PRODUCT_NAME"), 
						rs.getFloat("PRICE"), 
						rs.getInt("QUANTITY")));
			}
			
			//cleanup result set
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			//cleanup database
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return orders;
	}

	public Order findById(int id) {
		
		String sql = String.format("SELECT * FROM `testapp`.`orders` WHERE `ID`= %d;", id);
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			Order order = new Order();
			order.setId(rs.getInt(1));
			order.setOrderNo(rs.getString("ORDER_NO"));
			order.setProductName(rs.getString("PRODUCT_NAME"));
			order.setPrice(rs.getFloat("PRICE"));
			order.setQuantity(rs.getInt(1));
			return order;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return new Order();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean createT(Order t) {
		
		Connection conn = null;
		try {
			String sql = String.format("INSERT INTO `testapp`.`orders`(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('%s', '%s', %f, %d)", 
					t.getOrderNo(), 
					t.getProductName(), 
					t.getPrice(), 
					t.getQuantity());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			//really should check the return value from executeUpdate
			stmt.executeUpdate(sql);
			stmt.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public boolean updateT(Order t) {
		
		try {
			String sql = String.format("UPDATE `testapp`.`orders` SET `ORDER_NO` = '%s', `PRODUCT_NAME` = '%s', `PRICE` = '%f', `QUANTITY` = %d where ID=%d;",
					t.getOrderNo(),
					t.getProductName(),
					t.getPrice(),
					t.getQuantity(),
					t.getId());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean deleteT(Order t) {
		String sql = String.format("DELETE FROM `testapp`.`orders` WHERE `ID` = %d;",
				t.getId());
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
