package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@ManagedBean
@ViewScoped
public class User {
	@NotNull(message = "Please enter a First Name. This is a required field.")
	@NotBlank(message = "Please enter a First Name. This is a required field.")
	@Size(min=3, max=15)
	private String firstName = "";
	
	@NotNull(message = "Please enter a Last Name. This is a required field.")
	@NotBlank(message = "Please enter a Last Name. This is a required field.")
	@Size(min=3, max=15)
	private String lastName = "";
	
	@NotNull(message="You must put in a user name")
	@NotBlank(message="You must put in your username")
	@Size(min=3, max=20, message="Size must be between 3 and 20 characters long")
	private String userName;
	
	@NotNull(message="You must add an email")
	@NotBlank(message="You must add an email")
	@Size(max=100)
	@Email(message="This must be an email")
	private String email;
	
	@NotNull(message="You must put in a password")
	@NotBlank(message="You must put in a password")
	@Size(min=5, message="Size must be larger than 5")
	private String password;

	public User() {
		firstName="Cameron";
		lastName="Bryant";
		userName="cbryant";
		email="cameronbryant93@gmail.com";
		password="bryant";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
