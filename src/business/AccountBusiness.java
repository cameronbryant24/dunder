package business;

import beans.User;
import data.UserDataAccess;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Stateless
@Local(AccountBusinessInterface.class)
@LocalBean
public class AccountBusiness implements AccountBusinessInterface {
	
	@Inject
	UserDataAccess userData;
	
	public AccountBusiness() {
    }
	
    public int AuthenticateUser(User user) {
    	User dataUser = userData.findByUserName(user);
        user.setPassword(hashPassword(user.getPassword()));
        if(user.getPassword().equals(dataUser.getPassword())) {
        	return 1;
        } else { 
        	return 0; }
    }
    
    public int RegisterUsers(User user) {
    	user.setPassword(hashPassword(user.getPassword()));
        if(userData.createT(user)) {
        	return 1;
        }else {
        	return 0;
        }
    }

	public void updateUserInfo(User u) {
		u.setPassword(hashPassword(u.getPassword()));
		if(userData.updateT(u)) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().replace("user", u);
		}
	}

    public String hashPassword(String pass) {
    	try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(pass.getBytes());
			
			byte byteData[] = md.digest();
			
			StringBuffer sb = new StringBuffer();
			
			for(int i=0; i<byteData.length; i++)
			{
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			
			return sb.toString();
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
    }
    
    public UserDataAccess getUserData() {
    	return userData;
    }
}
