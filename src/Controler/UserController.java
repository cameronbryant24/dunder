package Controler;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.User;
import business.AccountBusiness;

@ManagedBean
@ViewScoped
public class UserController {

	@EJB
	AccountBusiness ab;
	
	public String onLogin(User user) {
		if(ab.AuthenticateUser(user) == 1) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", ab.getUserData().findByUserName(user));
			return "BrowseAll.xhtml?faces-redirect=true";
		}
		else {
			return "Login.xhtml";
		}
	}

	public String onRegister(User user) {
		if(ab.RegisterUsers(user) == 1) {
			return "Login.xhtml";
		} else {
			return "Register.xhtml";
		}
	}

	public String updateUserInfo(User u) {
		ab.updateUserInfo(u);
		return "UserInfo.xhtml";
	}
}
