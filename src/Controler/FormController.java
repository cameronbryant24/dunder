package Controler;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Date;

import beans.Order;
import beans.User;
import business.AccountBusiness;
import business.MyTimerService;
import business.OrdersBusinessInterface;
import business.OrdersBusinessService;

@ManagedBean
@ViewScoped
public class FormController {
	
	@Inject
	OrdersBusinessService service;
	
	@Inject
	AccountBusiness ab;
	
	public String onLogoff() {
		//Invalidate the session to clear the security token
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		
		//Redirect to a protexted page
		return "Login.xhtml?faces-redirect=true";
	}
	
	public String goToAddOrder() {
		return "Add.xhtml";
	}
	
	public String onDelete() {
		Order order=(Order) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("order");
		service.deleteOrder(order);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("order");
		return "TestResponse.xhtml";
	}
	
	public String onEdit(int orderId) {
		Order order=service.getOrderById(orderId);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", order);
		
		return "Update.xhtml";
	}
	
	public String onCancel() {
		return "TestResponse.xhtml";
	}
	
	public String updateProduct() {
		Order order=(Order) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("order");
		service.updateOrder(order);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("order");
		return "TestResponse.xhtml";
	}
	
	public String onSubmit(User user) {
		
		if (ab.AuthenticateUser(user)==1) {
			
		//forward t test response view along with the user managed bean
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
		return "TestResponse.xhtml";

		}
		else {
			return "ErrorPage.xhtml";
		}
	}
	
	public String onRegister(User user) {
		if(ab.RegisterUsers(user) == 1) {
			return "Login.xhtml";
		} else {
			return "Register.xhtml";
		}
	}
	
	public OrdersBusinessInterface getService() {
		return service;
	}
	
	public String addOrdertoDatabase(Order order) {
		service.saveOrder(order);
		return "TestResponse.xhtml";
	}
}
